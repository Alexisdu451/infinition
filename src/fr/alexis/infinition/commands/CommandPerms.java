package fr.alexis.infinition.commands;

import fr.alexis.infinition.manager.InfinitionPermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandPerms implements CommandExecutor {

    private final InfinitionPermissionManager perms = new InfinitionPermissionManager();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = Bukkit.getPlayer(commandSender.getName());
        if (args.length == 3 && args[1].equals("addgroup")) {
            if (commandSender.hasPermission("admin.perms")) {
                perms.setGroup(Bukkit.getPlayer(args[0]), args[2], player);
                return true;
            } else {
                commandSender.sendMessage("§bInfinition §6» §7Vous n'avez pas la permission!");
                return false;
            }

        }
        commandSender.sendMessage("§bInfinition §6» §7Erreur! : /perms <Pseudo> addgroup <group>");
        return false;
    }

}
