package fr.alexis.infinition.manager;

import fr.alexis.infinition.Infinition;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;

import static org.bukkit.Bukkit.getServer;

public class RankPointManager {

    public HashMap<Player, String> rank = new HashMap<>();

    /**
     * Permet de get les points par rapport a la categorie
     *
     * @param categorie la categorie de notation
     * @param player    le joueur ou vous voulez regarder les points
     * @return
     */
    public Integer getPointsForCategorie(String categorie, Player player) {
        return (Integer) Infinition.getDb().read("SELECT * FROM players_manager WHERE player_name='" + player.getName() + "'", categorie);
    }

    /**
     * Permet d'ajouter des points lors de la notation
     *
     * @param categorie la categorie de notation
     * @param player    le joueur ou vous voulez ajouter les points
     * @param points    le nombre de point que vous voulez ajouter
     */
    public void setPointsForCategorie(String categorie, Player player, int points) {
        int point = getPointsForCategorie(categorie, player) + points;
        Infinition.getDb().update("UPDATE players_manager SET " + categorie + "=" + point + " WHERE player_name='" + player.getName() + "'");
    }

    /**
     * Permet de retirer des points lors de la notation
     *
     * @param categorie categorie de notation
     * @param player    le joueur noté
     * @param points    point a attribue
     */
    public void remPointsForCategorie(String categorie, Player player, int points) {
        int point = getPointsForCategorie(categorie, player) - points;
        Infinition.getDb().update("UPDATE players_manager SET " + categorie + "=" + point + " WHERE player_name='" + player.getName() + "'");
    }

    /**
     * Reset tout les points d'un categorie
     *
     * @param categorie ou vous voulez reset les points
     * @param player    joueur ou les points sont reset
     */
    public void resetPointsForCategorie(String categorie, Player player) {
        Infinition.getDb().update("UPDATE players_manager SET " + categorie + "='0' WHERE player_name='" + player.getName() + "'");
    }

    /**
     * Permet de reset tout les points de toutes les categories
     *
     * @param player joueur ou les points sont reset
     */
    public void resetAllPointsForCategorie(Player player) {
        Infinition.getDb().update("UPDATE players_manager SET  Organique='0' WHERE player_name='" + player.getName() + "'");
        Infinition.getDb().update("UPDATE players_manager SET  Build='0' WHERE player_name='" + player.getName() + "'");
        Infinition.getDb().update("UPDATE players_manager SET  Terraforming='0' WHERE player_name='" + player.getName() + "'");
    }

    public void createRankPoint(Player player) {
        Infinition.getDb().update("INSERT INTO players_manager (uuid, player_name) VALUES ('" + player.getUniqueId() + "','"+ player.getName() + "')");
    }

    public boolean isExist(Player player) {

        ResultSet rs = Infinition.getDb().getResult("SELECT * FROM players_manager WHERE player_name='" + player.getName() + "'");
        try {
            return rs.next();
        } catch (SQLException e) {
            getServer().getLogger().log(Level.WARNING, e.getMessage(), e);
        }

        return false;
    }

    public String getRankSQL(Player player) {
        return (String) Infinition.getDb().read("SELECT * FROM players_manager WHERE player_name='" + player.getName() + "'", "rank");
    }
    public void addRank(Player player){
        this.rank.put(player, getRankSQL(player));
    }
    public void remRank(Player player){
        this.rank.remove(player);
    }
    public String getRank(Player player){
        return this.rank.get(player);
    }


    public void setRank(Player player, String rank) {
        Infinition.getDb().update("UPDATE players_manager SET rank='" + rank + "' WHERE player_name='" + player.getName() + "'");
    }


}
