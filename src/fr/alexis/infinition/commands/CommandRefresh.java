package fr.alexis.infinition.commands;

import fr.alexis.infinition.manager.InfinitionPermissionManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandRefresh implements CommandExecutor {
    private final InfinitionPermissionManager perms = new InfinitionPermissionManager();
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = Bukkit.getPlayer(commandSender.getName());
        if(player.hasPermission("infinition.admin")){
            for(Player pls : Bukkit.getOnlinePlayers())
                perms.refreshPerm(pls, player);

        } else
            player.sendMessage("§bInfinition §6» §7Vous n'avez pas la permission!");

        return false;
    }
}
