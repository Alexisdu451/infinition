package fr.alexis.infinition.commands;

import fr.alexis.infinition.manager.RankPointManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankPointCommand implements CommandExecutor {

    private final RankPointManager rankPointManager = new RankPointManager();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length >= 1) {
            Player player = Bukkit.getPlayer(args[0]);

            if (args.length == 1) {
                sender.sendMessage("§6--------------[§bInfinition : " + Bukkit.getPlayer(args[0]).getDisplayName() + "§6]--------------\n\n§bOrganique §6» §7" + rankPointManager.getPointsForCategorie("Organique", player) + " points\n§bBuild §6» §7" + rankPointManager.getPointsForCategorie("Build", player) + " points\n§bTerraforming §6» §7" + rankPointManager.getPointsForCategorie("Terraforming", player) + " points\n\n§6--------------[§bInfinition : " + Bukkit.getPlayer(args[0]).getDisplayName() + "§6]--------------");
                return true;
            }

            if (args.length == 2 && (args[1].equals("Terraforming") || args[1].equals("Organique") || args[1].equals("Build"))) {
                sender.sendMessage("§6--------------[§bInfinition : " + Bukkit.getPlayer(args[0]).getDisplayName() + "§6]--------------\n\n§b" + args[1] + " §6» §7" + rankPointManager.getPointsForCategorie(args[1], player) + " points\n\n§6--------------[§bInfinition : " + Bukkit.getPlayer(args[0]).getDisplayName() + "§6]--------------");
                return true;
            }
            if (args.length == 4 && (args[1].equals("Terraforming") || args[1].equals("Organique") || args[1].equals("Build")) && args[2].equals("add")) {
                rankPointManager.setPointsForCategorie(args[1], player, Integer.parseInt(args[3]));
                sender.sendMessage("§bInfinition §6» §7Vous venez d'ajouter §b" + args[3] + "§7 points au joueur §b" + Bukkit.getPlayer(args[0]).getDisplayName() + "§7 dans la catégorie §b" + args[1]);
                return true;
            }
            if (args.length == 4 && (args[1].equals("Terraforming") || args[1].equals("Organique") || args[1].equals("Build")) && args[2].equals("remove")) {
                rankPointManager.remPointsForCategorie(args[1], player, Integer.parseInt(args[3]));
                sender.sendMessage("§bInfinition §6» §7Vous venez de retirer §b" + args[3] + "§7 points au joueur §b" + Bukkit.getPlayer(args[0]).getDisplayName() + "§7 dans la catégorie §b" + args[1]);
                return true;
            }
            if (args.length == 3 && (args[1].equals("Terraforming") || args[1].equals("Organique") || args[1].equals("Build")) && args[2].equals("reset")) {
                rankPointManager.resetPointsForCategorie(args[1], player);
                sender.sendMessage("§bInfinition §6» §7Vous venez de retirer tout les points au joueur §b" + Bukkit.getPlayer(args[0]).getDisplayName() + "§7 dans la catégorie §b" + args[1]);
                return true;
            }
            if (args.length == 2 && args[1].equals("reset")) {
                rankPointManager.resetAllPointsForCategorie(player);
                sender.sendMessage("§bInfinition §6» §7Vous venez de retirer tout les points au joueur §b" + Bukkit.getPlayer(args[0]).getDisplayName() + "§7 dans toutes les catégories");
                return true;
            }
        }


        sender.sendMessage("§bInfinition §6» §7Vous êtes tromper dans la commande!");
        return false;
    }
}
