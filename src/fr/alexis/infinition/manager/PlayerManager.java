package fr.alexis.infinition.manager;

import fr.alexis.infinition.scoreboard.TeamTag;
import org.bukkit.entity.Player;

public class PlayerManager {
    private final InfinitionPermissionManager perms = new InfinitionPermissionManager();

    public void displayGrouponTab(Player player){
        new TeamTag(1 + perms.findIDByGroup(perms.getGroup(player)) + player.getName(), perms.getPrefixSQL(perms.getGroup(player)),"" ).set(player);
    }
    public void onQuitDisplayTab(Player player){
        new TeamTag(1 + perms.findIDByGroup(perms.getGroup(player)) + player.getName(), perms.getPrefixSQL(perms.getGroup(player)),"" ).remove(player);
    }
}
