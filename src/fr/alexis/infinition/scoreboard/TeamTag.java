package fr.alexis.infinition.scoreboard;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Collection;
import java.util.logging.Level;

@Getter
public class TeamTag {
    private String prefix;
    private String suffix;
    @Setter
    private Team team;
    public final Scoreboard scoreboard;

    public TeamTag(String name, String prefix, String suffix, Scoreboard current) {
        this.prefix = prefix;
        this.suffix = suffix;
        this.scoreboard = current;
        this.team = current.getTeam(name);

        //Register if null
        if (team == null)
            team = current.registerNewTeam(name);

        team.setCanSeeFriendlyInvisibles(false);
        team.setAllowFriendlyFire(false);
        team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

        //Prefix < 16
        if (!prefix.isEmpty() && prefix.length() < 16 && suffix.length() < 16) {
            team.setPrefix(prefix);
            team.setSuffix(suffix);
        } else
            Bukkit.getLogger().log(Level.INFO, "Le préfixe ou le suffixe ne peut dépasser 16 caractères.");
    }


    public TeamTag(String name, String prefix, String suffix) {
        this(name, prefix, suffix, Bukkit.getScoreboardManager().getMainScoreboard());
    }

    @Deprecated
    public void set(Player p) {
        team.addPlayer(p);
        p.setScoreboard(scoreboard);
    }

    public void set(Collection<Player> players) {
        for (Player p : players)
            set(p);
    }

    @Deprecated
    public void remove(Player player) {
        team.removePlayer(player);
    }
    public void remove(Collection<Player> players) {
        for (Player p : players)
            remove(p);
    }

    public void removeTeam() {
        team.unregister();
    }


}
