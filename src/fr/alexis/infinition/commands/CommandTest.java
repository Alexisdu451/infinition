package fr.alexis.infinition.commands;

import fr.alexis.infinition.manager.InfinitionPermissionManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandTest implements CommandExecutor {

    private final InfinitionPermissionManager perms = new InfinitionPermissionManager();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        commandSender.sendMessage(String.valueOf(perms.isGroupExist(strings[0])));
        return false;
    }
}
