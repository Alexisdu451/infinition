package fr.alexis.infinition.manager;

import fr.alexis.infinition.Infinition;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class InfinitionPermissionManager {
    public HashMap<UUID, PermissionAttachment> playerPermissions = new HashMap<>();
    public HashMap<UUID, String> prefix = new HashMap<>();

    public void setupPermissions(Player player, String group) {
        PermissionAttachment attachment = player.addAttachment(Infinition.getInstance());
        this.playerPermissions.put(player.getUniqueId(), attachment);
        permissionsSetter(player.getUniqueId(), group);
    }

    public List<String> getPermissionWithGroup(String group) {
        return Infinition.getDb().readList("SELECT * FROM groups_permissions WHERE groups_name='" + group + "'", "permissions");
    }
    public String getPrefixSQL(String group){
        return (String) Infinition.getDb().read("SELECT * FROM groups_info WHERE groups_name='" + group + "'", "prefix");
    }
    public void setPrefix(Player player, String group){
        this.prefix.put(player.getUniqueId(), getPrefixSQL(group));
    }
    public void remPrefix(Player player){
        this.prefix.remove(player.getUniqueId());
    }
    public String getPrefix(Player player){
        return this.prefix.get(player.getUniqueId());
    }
    public void refreshPerm(Player player, Player commandSender){
        permissionRemove(player.getUniqueId());
        setupPermissions(player, getGroup(player));
        commandSender.sendMessage("§bInfiniton §6» §7Les grades et permission viennent d'être refresh!");
    }

    public String getGroup(Player player) {
        return (String) Infinition.getDb().read("SELECT * FROM players_manager WHERE player_name='" + player.getName() + "'", "groups");
    }
    public String findGroupByID(int id){
        return (String) Infinition.getDb().read("SELECT * FROM groups_info WHERE id='" + id + "'", "groups_name");
    }
    public Integer findIDByGroup(String group){
        return (Integer) Infinition.getDb().read("SELECT * FROM groups_info WHERE groups_name='" + group + "'", "id");
    }

    public void setGroup(Player player, String group, Player commandSender) {
        if (isGroupExist(group)) {
            remPrefix(player);
            Infinition.getDb().update("UPDATE players_manager SET groups='" + group + "' WHERE player_name='" + player.getName() + "'");
            System.out.print("[InfinitionPermissionManager] Ajout du group " + group + " au joueur " + player.getName());
            commandSender.sendMessage("§bInfinition §6» §7Vous venez d'ajouter §b" + player.getName() + "§7 au group §b" + group);
        } else {
            commandSender.sendMessage("§bInfinition §6» §7Le group §b" + group + "§7 n'existe pas !");
            commandSender.sendMessage("§4Voici la liste des groups " + getGroupList().toString());

        }

    }

    public List<String> getGroupList() {
        return Infinition.getDb().readList("SELECT * FROM groups_info", "groups_name");
    }

    public void permissionRemove(UUID uuid) {
        PermissionAttachment attachment = this.playerPermissions.get(uuid);
        getLogRemove(uuid);
        attachment.remove();
    }

    public void getLogSetter(String group, UUID uuid, String permissions) {
        System.out.print("[InfinitionPermissionManager] Ajout Permission  : " + permissions + " du group " + group + " au joueur " + Bukkit.getPlayer(uuid).getName());
    }

    public void getLogRemove(UUID uuid) {
        System.out.print("[InfinitionPermissionManager] Ajout Permission  : Remove de toutes les permissions de " + Bukkit.getPlayer(uuid).getName());
    }

    public boolean isGroupExist(String groups) {
        if (getGroupList().contains(groups))
            return true;

        return false;
    }

    private void permissionsSetter(UUID uuid, String group) {
        PermissionAttachment attachment = this.playerPermissions.get(uuid);

        for (String permissions : getPermissionWithGroup(group)) {
            getLogSetter(group, uuid, permissions);
            attachment.setPermission(permissions, true);
        }

    }


}
