package fr.alexis.infinition;

import fr.alexis.infinition.commands.CommandPerms;
import fr.alexis.infinition.commands.CommandRefresh;
import fr.alexis.infinition.commands.CommandTest;
import fr.alexis.infinition.commands.RankPointCommand;
import fr.alexis.infinition.listener.PlayerListener;
import fr.alexis.infinition.manager.Database;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class Infinition extends JavaPlugin {

    private static Database db;
    public static Infinition infinition;

    public void onEnable(){
        infinition = this;
        db = new Database("localhost", "test", "root", "");

        getCommand("rankpoint").setExecutor(new RankPointCommand());
        getCommand("perms").setExecutor(new CommandPerms());
        getCommand("test").setExecutor(new CommandTest());
        getCommand("refresh").setExecutor(new CommandRefresh());

        registerEvents(new PlayerListener());

        Bukkit.getLogger().log(Level.INFO, "Le plugin Infinition vient de charger!");
    }

    public static Database getDb(){
        return db;
    }

    public static Infinition getInstance(){
        return infinition;
    }
    private void registerEvents(Listener... listeners) {
        PluginManager pm = Bukkit.getPluginManager();
        for (Listener l : listeners) {
            pm.registerEvents(l, this);
        }
    }

}
