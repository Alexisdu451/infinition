package fr.alexis.infinition.listener;

import fr.alexis.infinition.manager.InfinitionPermissionManager;
import fr.alexis.infinition.manager.PlayerManager;
import fr.alexis.infinition.manager.RankPointManager;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.Collections;

public class PlayerListener implements Listener {

    public ArrayList<Integer> points = new ArrayList<>();
    private final RankPointManager rankPointManager = new RankPointManager();
    private final InfinitionPermissionManager perms = new InfinitionPermissionManager();
    private final PlayerManager playerManager = new PlayerManager();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!rankPointManager.isExist(player))
            rankPointManager.createRankPoint(player);

        //setup du système de permission
        perms.setupPermissions(player, perms.getGroup(player));
        perms.setPrefix(player, perms.getGroup(player));
        rankPointManager.addRank(player);
        playerManager.displayGrouponTab(player);

        if (player.hasPermission("infinition.join"))
            event.setJoinMessage(perms.getPrefix(player) + player.getName() + "§e à rejoint le serveur!");
        else
            event.setJoinMessage(null);


        //gestion ranked
        if (player.hasPermission("infinition.ranked")) {
            points.add(0, rankPointManager.getPointsForCategorie("Organique", player));
            points.add(1, rankPointManager.getPointsForCategorie("Terraforming", player));
            points.add(2, rankPointManager.getPointsForCategorie("Build", player));

            Collections.sort(points, Collections.reverseOrder());

            if (rankPointManager.getPointsForCategorie("Organique", player).equals(points.get(0)))
                rankPointManager.setRank(player, "Maître Organique");
            if (rankPointManager.getPointsForCategorie("Build", player).equals(points.get(0)))
                rankPointManager.setRank(player, "Maître Build");
            if (rankPointManager.getPointsForCategorie("Terraforming", player).equals(points.get(0)))
                rankPointManager.setRank(player, "Maître Terraforming");
        }


    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (player.hasPermission("infinition.join"))
            event.setQuitMessage(perms.getPrefix(player) + player.getName() + "§e à quitté le serveur!");
        else
            event.setQuitMessage(null);

        //gestion des permissions
        playerManager.onQuitDisplayTab(player);
        perms.permissionRemove(player.getUniqueId());
        perms.remPrefix(player);
        rankPointManager.remRank(player);
    }


    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        event.setCancelled(true);
        if(player.hasPermission("infintion.ranked.show")){
            if(player.hasPermission("infinition.ranked")){
                TextComponent name = new TextComponent(perms.getPrefix(player) + player.getName() + " §6» §7" + event.getMessage());
                name.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§bRank §6» §7" + rankPointManager.getRank(player) + "\n§bNiveau de Build §6» §7Nouveau").create()));
                player.spigot().sendMessage(name);
            } else {
                TextComponent name_1 = new TextComponent(perms.getPrefix(player) + player.getName() + " §6» §7" + event.getMessage());
                name_1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§bNiveau de Build §6» §7Nouveau").create()));
                player.spigot().sendMessage(name_1);
            }
        } else
            player.sendMessage(perms.getPrefix(player) + player.getName() + " §6» §7" + event.getMessage());



    }
}
